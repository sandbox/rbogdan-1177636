<?php

/**
 * @file
 * Manage faq module settings.
 */

function faq_with_asking_faq_unanswered_questions() {

  $header = array(t('Question'), t('Answer'), t('Delete'));
  $rows = array();
  $dest = drupal_get_destination();
  $result = db_query("SELECT nid, title FROM {node} WHERE type = 'faq' AND status = 0 ORDER BY nid DESC");
  foreach ($result as $record) {
    $row = array();
    $row[] = check_plain($record->title);
    $row[] = l(t('Answer'), 'node/'. $record->nid .'/edit', array('query' => $dest));
    $row[] = l(t('Delete'), 'node/'. $record->nid .'/delete', array('query' => $dest));;
    $rows[] = array(
      'data' => $row,
    );
  }
  $output = theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('id' => 'question-sort')));
  return $output;
}
