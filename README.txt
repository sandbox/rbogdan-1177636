CONTENTS OF THIS FILE
----------------------

  * Introduction
  * Installation
  * Configuration


INTRODUCTION
------------
Maintainer: Bogdan Romanenko (http://drupal.org/user/375809)

Documentation: http://drupal.org/node/129209

The FAQ with easy asking functional module allow users with the 'create faq
question' permission add new FAQ node fill only question field. Users with
'create faq answer' permission can add answer on this question and publish node.
Also they can admin questions on page faq/unanswered.


INSTALLATION
------------
1. Copy faq_asking folder to modules directory.
2. At admin/build/modules enable the faq module.
3. Enable permissions at admin/people/permissions.



